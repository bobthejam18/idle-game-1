// Let Statements
let money = 0;
let autoClickerCost = 5;
let autoClickers = 0;
let autoClickerInterval = setInterval(autoClicker, 1000);
let superAutoClickerCost = 10;
let superAutoClickers = 0;
let superAutoClickerInterval = setInterval(superAutoClicker, 400);

// Money Button
function moneyButtonClick() {
	money += 1;

	document.getElementById("money").innerHTML = money;
};

// Purchase AutoClicker Button
function purchaseAutoClickerButtonClick() {
	if (money >= autoClickerCost) {
		money -= autoClickerCost;
		autoClickers += 1;
		autoClickerCost += 5 * autoClickers;

		document.getElementById("money").innerHTML = money;
		document.getElementById("autoClickers").innerHTML = autoClickers;
		document.getElementById("autoClickerCost").innerHTML = autoClickerCost;

		clearInterval(autoClickerInterval);

        if (autoClickers > 0) {
            autoClickerInterval = setInterval(autoClicker, 1000 / autoClickers);
        } else {
            autoClickerInterval = setInterval(autoClicker, 1000);
        }
	}
};

// AutoClicker
function autoClicker() {
	if (autoClickers >= 1) {
		money += 1;
		
		document.getElementById("money").innerHTML = money;
		document.getElementById("autoClickers").innerHTML = autoClickers;
	}
};

// Purchase Super AutoClicker Button
function purchaseSuperAutoClickerButtonClick() {
	if (money >= superAutoClickerCost) {
		money -= superAutoClickerCost;
		superAutoClickers += 1;
		superAutoClickerCost += 15 * superAutoClickers;

		document.getElementById("money").innerHTML = money;
		document.getElementById("superAutoClickers").innerHTML = superAutoClickers;
		document.getElementById("superAutoClickerCost").innerHTML = superAutoClickerCost;

		clearInterval(superAutoClickerInterval);

        if (superAutoClickers > 0) {
            superAutoClickerInterval = setInterval(superAutoClicker, 400 / superAutoClickers);
        } else {
            superAutoClickerInterval = setInterval(superAutoClicker, 400);
        }
	}
};

// Super AutoClicker
function superAutoClicker() {
	if (superAutoClickers >= 1) {
		money += 1;
		
		document.getElementById("money").innerHTML = money;
		document.getElementById("superAutoClickers").innerHTML = autoClickers;
	}
};
